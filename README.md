*Introduction to Proofs* is an undergraduate text. 
It is Inquiry-Based, sometimes called the Discovery Method or the Moore Method. 
It is Freely available.

Highlights
==========

+ Inquiry-based  
    
The text is a sequence of statements for students to prove, along with a few definitions and remarks. 
The instructor does not lecture but instead lightly guides as the class works through the material together. 
For these students, this is the best way to develop mathematical maturity.
    
+ Covers needed material  
    
We do elementary number theory to the Fundamental Theorem of Arithmetic, sets to DeMorgan's Laws, 
functions to two-sided inverses, and relations up to equivalences and partitions.
    
+ No prerequisite  
    
We enroll sophomore Math majors who typically have taken Calculus III and Linear Algebra. 
But this background isn't a logical prerequisite, it just ensures that students have some mathematical aptitude. 
The material does not presume any college mathematics.
    
+ Free  
    
You can the text without cost. 
You can share it, for instance with students, without cost.
This includes the LaTeX source, so if you are an instructor then you can tune the text to your class.

+ Flexibility  
    
The text comes in two presentations, one of which is compact enough to just print and hand out on the first day, which is what I do. 
There are a number of other options; see the source.
    
+ Extras  
    
There is a pack of beamer slides on logic. 
Tese organize the discussions that arise naturally in the first couple of weeks about topics such as the definition of implication.
    
There is also a pack of slides I show on the first day to introduce students to the class style.