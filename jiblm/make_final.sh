#!/bin/sh
#
# make_final.sh
#  Make a final version
# 2021-Aug-03 JH
pdflatex hefferon_final
pdflatex hefferon_final
zip hefferon_final hefferon_final.pdf hefferon_final.tex asy/*.pdf \
    JIBLM.tex submission_form.doc
