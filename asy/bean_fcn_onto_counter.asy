// size(6cm,6.5cm);
unitsize(1cm);
// Colors from "Still not getting any" by sp613g, https://kuler.adobe.com/#themeID/181115
import ibl;
// Make a bean picture of a set

pair codomain_offset=(4,0);
transform codomain_shift=shift(codomain_offset);
transform cocodomain_shift=shift(2*codomain_offset);
filldraw(bean,fillpen=color_base);
filldraw(codomain_shift*bean,fillpen=color_base);
filldraw(cocodomain_shift*bean,fillpen=color_base);
// label("$\mapsvia{f}$",(2,1.5),E);

// Points
real r=.1cm;
real bar_length=2.5pt;
// real label_offset_x=0.12*r, label_offset_y=0.23*r;
// label("{$D$}",bean1+(label_offset_x,label_offset_y));            
// label("{$C$}",codomain_shift*bean1+(label_offset_x,label_offset_y));
pair zero=bean1+(0.14*r,0.20*r);
pair one=bean0+(0.18*r,0.20*r);
dot("{$0$}",zero,align=1.5*W);
dot("{$1$}",one,align=1.5*W);
pair a=codomain_shift*(bean1+(0.14*r,0.26*r));
pair b=codomain_shift*(bean0+(0.18*r,0.35*r));
pair c=codomain_shift*(bean0+(0.12*r,0.10*r));
dot("{$10$}",a,align=1.5*N);
dot("{$11$}",b,align=1.5*N);
dot("{$12$}",c,align=1.5*N);
pair alpha=codomain_shift*codomain_shift*(bean1+(0.16*r,0.20*r));
pair beta=codomain_shift*codomain_shift*(bean0+(0.14*r,0.20*r));
dot("{$20$}",alpha,align=1.5*E);
dot("{$21$}",beta,align=1.5*E);

//Function associations between points
path zero_to_b = zero{dir(20)}..b;
path one_to_c = one{dir(20)}..c;

path zero_to_b_drawn = subpath(zero_to_b,0.06,0.94);  // I don't think bar takes account of Margin
draw(zero_to_b_drawn,black,BeginBar(bar_length),Arrow(TeXHead),margin=NoMargin);
path one_to_c_drawn = subpath(one_to_c,0.06,0.94);  
draw(one_to_c_drawn,black,BeginBar(bar_length),Arrow(TeXHead),margin=NoMargin);

path a_to_alpha = a{dir(20)}..alpha;
path b_to_alpha = b{dir(20)}..alpha;
path c_to_beta = c{dir(20)}..beta;

path a_to_alpha_drawn = subpath(a_to_alpha,0.06,0.94);  // I don't think bar takes account of Margin
draw(a_to_alpha_drawn,black,BeginBar(bar_length),Arrow(TeXHead),margin=NoMargin);
path b_to_alpha_drawn = subpath(b_to_alpha,0.06,0.94);  
draw(b_to_alpha_drawn,black,BeginBar(bar_length),Arrow(TeXHead),margin=NoMargin);
path c_to_beta_drawn = subpath(c_to_beta,0.06,0.94);  
draw(c_to_beta_drawn,black,BeginBar(bar_length),Arrow(TeXHead),margin=NoMargin);

// Functions
pair f_start=interp(bean2,bean5,0.5)+(0,0.25);
pair f_end=codomain_shift*f_start;
pair g_start=f_end;
pair g_end=codomain_shift*f_end;
path f = f_start{dir(30)}..f_end;
path f_drawn = subpath(f,0.25,0.75);  // 
draw("{$f$}",f_drawn,LeftSide,black,ArcArrow,NoMargin);
path g = g_start{dir(30)}..g_end;
path g_drawn = subpath(g,0.25,0.75);  // 
draw("{$g$}",g_drawn,LeftSide,black,ArcArrow,NoMargin);
