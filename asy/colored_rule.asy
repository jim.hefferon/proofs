// colored_rule.asy
//  Graphic for *An Inquiry-based Introducion to Proof* by Jim Hefferon
// 2018-Aug-07 JH PD
import settings;
settings.tex="xelatex";
settings.outformat="pdf";

unitsize(0.8cm);
import ibl;

real wd = 12;
real thickness = 1;
int number_shades = 100;

for(int i=0; i < number_shades; ++i) {
  pen p = squarecap 
    + linewidth(thickness)
    + color_light2
    + opacity((number_shades-i)/number_shades);
    draw((wd*i/number_shades,0)
	 --(wd*(i+1)/number_shades,0),p);
}
