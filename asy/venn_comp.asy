size(3cm,2cm);
import ibl;

pair z0=(0,0);
pair z1=(0,0);  // center of circle1
real r=1.5;     // radius
path c1=circle(z1,r);
fill(c1,color_base);

draw(c1);

picture a_box=new picture;
real label_offset_x=0.4*r, label_offset_y=0.5*r;
label(a_box,"{\scriptsize $A$}",z1+(-1*label_offset_x,label_offset_y));
add(a_box,filltype=Fill(white));

draw((-1-r,0)--(1+r,0),nullpen); // empty line to make universal box same size as other two

shipout(bbox(0.25cm, filltype=FillDraw(fillpen=color_intermediate)));
