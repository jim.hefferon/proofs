\documentclass[11pt]{article}
\usepackage[left=1.5in, right=1.5in]{geometry}
\usepackage{mathtools,amssymb,usualjh}
\usepackage{fourier}
\usepackage[T1]{fontenc}

\renewcommand{\part}[1]{\medskip\par\noindent\textbf{#1}\hspace{0.2em} }

\setlength{\parskip}{0.3ex plus 0.1ex}
\pagestyle{empty}
\begin{document}\thispagestyle{empty}
\begin{center}
  {\large\textbf{Writing Mathematics}}  \\[1.5ex]
  Jim Hef{}feron  \\[0.25ex]
  St Michael's College, Colchester, VT USA
\end{center}
\vspace{1cm}

In grade school, in high school, and into Calculus, you took 
classes with people who will not become mathematicians.
But starting now, you will be trained as a professional.

Professionals don't write this.
\begin{equation*}
  0=1\;\therefore\text{solutions}=\emptyset
\end{equation*}
They instead write this.
\begin{quotation}
\noindent
After bringing the system to echelon form, one of the equations is
$0=1$.
Thus the system has no solution.
\end{quotation}

The person who wrote the first may respond, ``You know what I mean.''
They are inadvertently revealing something:~they think of themself as
a student, doing the exercises that they've been assigned.
In a professional context, 
it is not for the reader to figure out what you mean, it is for you to 
say what you mean.
Our standard requires writing things that are correct,
but we also write them clearly.
Learning that is a lifelong pursuit, but here are some first
lessons.


\part{All writing has an audience}
In here, your audience is your peers.
You don't need to justify the quadratic formula 
or that multiplying by a negative number reverses an inequality,
everyone else knows that.
But many things they don't know, and those you do need to justify.


\part{Follow the basic rules of rhetoric} 
Write sentences.
Start them with a capital letter, finish them with a period, and
in the middle have them follow a reasonable structure.

Where appropriate, break the sentences into paragraphs.
The logical structure of your argument may suggest something;~for 
an `if and only if' proof, 
you could have an `if' paragraph and an `only if' paragraph, or if your
proof has several cases then they may make natural breaks,
and an induction argument has a base step and an inductive step.
In general, if you have more four or five sentences then
look for a way to chunk.

The paragraphs provide structure.
Math is hard.
If you have two paragraphs and the first starts ``We begin by \ldots{}'' while
the second has ``We finish by \ldots{}'' then you have done a good thing.

In Mathematics, the pronoun is ``we'' and the tense is present.
Don't write ``I found the roots,'' instead say 
``We next find the roots.''
Even better, if you are facile enough to pull it off, is to
be imperative with ``Find the roots.'' 


\part{Symbols}
Never start a sentence with a symbol.
If you want to say ``$\!x$ is even'' and can't think of a better way 
to avoid it, 
then write ``The number $x$ is even.''

When in doubt, use fewer symbols.
Prefer ``Thus $x$ is even'' to ``Thus $x=2k$ for some $k\in\N$,''
unless you are introducing $k$ to refer to it later.
In particular, the only time to use the
symbol `$\therefore$' is in a sentence warning others not to use it.
Another suspect is `$\Longrightarrow$'. 


\part{Proving is not teaching}
So far, you have seen mathematical presentations only 
in the context of someone teaching.
Proving is different.
 
Mathematical professionals would not write 
``At first I tried Sperner's Lemma,'' as part of a proof, or even
``Sperner's Lemma won't work because \ldots''\spacefactor=3000{}
Similarly, you should not say, 
``The point of dividing by $2x$ is \ldots''\spacefactor=3000{}
If in a document you have reason to
include motivation then that's fine,
but it goes outside the proof.
In this class, we generally won't include such
outside material.

Also don't say ``The square of an even number is even because 
for instance $2^2=4$ and $10^2=100$.'' 
Developing intuition with examples is sound teaching but poor proving.
Instead, you must provide a general argument along the lines of 
``The square of an even number is even because $(2k)^2=2\cdot 2k^2$.''

That is, in contrast with your past experience with teachers, 
proof-writers do not feel obliged to say what they are doing.
The culture of Mathematics
is that, for instance, to prove that if the difference of two numbers is even
then so is their sum, a writer may say ``If $m,n\in\Z$ are such that
$m-n$ is even,
then $n+m=(n-m)+2m$ is the sum of two evens, and is therefore also even.''
They do not explain how they thought to introduce the $2m$, 
or what else would work, or whether this extends to multiples of three.
Later, when you are working through some tough argument in 
Abstract Algebra or Real Analysis, you may well think that 
things would be better if there were more motivation and explanation.
That is a conversation for another forum.
Today, for professional communication in our field,
this is the standard.
\end{document}
