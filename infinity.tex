\chapter{Infinity}

Recall Exercise~\ref{CorrespondingSetsHaveSameNumberOfElements},
that if two finite sets correspond then they have the same number of elements.

\begin{df}
Two sets, finite or infinite, have the \definend{same cardinality}, 
or are \definend{equinumerous}, if there is a 
correspondence from one to the other.
We write $A\sim B$.
\end{df}

\begin{problem} \label{ex:EqCardOfSomePairs}
  For each, prove that the two have the same cardinality.
  \begin{items}
  \item $\N$ and $E=\set{2k\suchthat k\in\N}$
  \item $\open{-\pi/2}{\pi/2}$ and $\R$
  \item $\open{0}{1}$ and $\R$
  \end{items}
  \begin{answer}
    \begin{items}
    \item The function $\map{f}{\N}{E}$ given by $f(n)=2n$ is
      one-to-one and onto.
    \item The function $\map{\tan}{\open{-pi/2}{\pi/2}}{\R}$ is one-to-one
      and onto.
    \item Consider the map $\map{t}{\open{0}{1}}{\R}$ given by 
      $t(x)=\tan(\pi(x-(1/2)))$.
    \end{items}
  \end{answer}
\end{problem}

\begin{problem} \label{ex:EquinumeruousIsEquivalence}
Prove that the relation~$\sim$ is an equivalence.
\begin{answer}
  To show that the relation~$\sim$ is reflexive 
  suppose that $A$ is a set and note that the 
  identity function~$\map{\identity}{A}{A}$ is a correspondence.
  To show that the relation is symmetric recall 
  Exercise~\ref{PropertiesOfInverses}, that
  any correspondence~$\map{f}{A}{B}$ has an inverse~$\map{f^{-1}}{B}{A}$
  that is also a correspondence.

  To prove that the relation~$\sim$ is transitive assume that 
  $A\sim B$ and~$B\sim C$.
  Then there is a correspondence~$\map{f}{A}{B}$ and a 
  correspondence $\map{g}{B}{C}$.
  The composition $\map{\composed{g}{f}}{A}{C}$ is a correspondence
  by Exercise~\ref{InteractionOneToONeOntoWithComposition}.
  Thus $A\sim B$.
\end{answer}
\end{problem}

\begin{df}
A set is \definend{finite} if it has~$n$ elements for some $n\in\N$,
that is, if it has the same cardinality as some initial segment 
$\setbuilder{i\in\N}{i< n}=\set{0,1,\ldots,n-1}$ of the natural numbers.
Otherwise the set is \definend{infinite}.   
% \end{df}\begin{df}
A set is \definend{denumerable} if it has the same cardinality 
as $\N$.
A set is \definend{countable} if it is either finite or denumerable.
\end{df}

\begin{problem}  \label{ex:IntegersCountable}
Prove each.
\begin{exes}
\begin{exercise} 
  The set of integers is countable.
\end{exercise}
\begin{answer}
  Consider the map $\map{f}{\N}{\Z}$ 
  that alternates between positive and negative values:
  $0\mapsunder{}0$, $1\mapsunder{}1$, $2\mapsunder{}-1$, 
  $3\mapsunder{}2$, $4\mapsunder{}-2$, $5\mapsunder{}3$, \ldots\,.
  More formally, $f(n)=-n/2$ if $n$ is even and $f(n)=(n+1)/2$ if~$n$ is odd.
  It is clearly a correspondence.
\end{answer}
\begin{exercise} 
  The set~$\N\times\N$ is countable.
\end{exercise}
\begin{answer}
  As in the prior item we will define a map
  $\map{f}{\N}{\N\times\N}$.
  The function alternates between columns and rows.
  It first covers the outputs $\sequence{x,y}$ where $x+y=0$, then those
  where $x+y=1$, etc.
  Here are the first few values.
  \begin{center}
    \begin{tabular}{r|cccccccccc}
      \textit{Input $n$}  &$0$ &$1$ &$2$ &$3$
                  &$4$ &$5$
                  &$6$ &$7$ &$8$ &$\cdots$  \\ \cline{2-11}
      \rule{0em}{2.25ex}
      \textit{Output $f(n)$} &$\seq{0,0}$ &$\seq{0,1}$ &$\seq{1,0}$ &$\seq{0,2}$
                  &$\seq{1,1}$ &$\seq{2,0}$
                  &$\seq{0,3}$ &$\seq{1,2}$ &$\seq{2,1}$ &$\cdots$ 
    \end{tabular}
  \end{center}
  This is clearly a correspondence.
  
  \remark a sketch helps.
  The values associated with successive arguments trace out the 
  diagonals of the array~$\N\times\N$.
  \begin{center}
  \newcommand{\chanpoint}[1]{\makebox[0em]{$(#1)$}}
  \newcommand{\chanarrow}{{\color{darkii} $\searrow$}}
  \setlength{\unitlength}{0.45in}
  \begin{picture}(4,4)
    \put(0,0){\chanpoint{0,0}}
    \put(1,0){\chanpoint{1,0}}
    \put(2,0){\chanpoint{2,0}}
    \put(3,0){\chanpoint{3,0}}
    \put(3.5,0){\ldots}
    \put(0,1){\chanpoint{0,1}}
    \put(1,1){\chanpoint{1,1}}
    \put(2,1){\chanpoint{2,1}}
    \put(2.75,1){\ldots}
    \put(0,2){\chanpoint{0,2}}
    \put(1,2){\chanpoint{1,2}}
    \put(2,2){\chanpoint{2,2}}
    \put(2.75,2){\ldots}
    \put(0,3){\chanpoint{0,3}}
    \put(1,3){\chanpoint{1,3}}
    % \put(2,3){\ldots}
    \put(0,3.6){$\vdots$}
    \put(1,3.6){$\vdots$}
    \put(2.5,3.1){$\iddots$}
    % arrows
    \put(0.5,0.5){\chanarrow}
    \put(0.5,1.5){\chanarrow}
    \put(1.5,0.5){\chanarrow}
    \put(0.5,2.5){\chanarrow}
  \end{picture}
  \end{center}  
\end{answer}
\end{exes}
% \begin{exes}
% \item The set of integers is countable.
% \item The set~$\N\times\N$ is countable.
% \end{exes}
% \begin{ans}
% \begin{exes}
% \item We will define a map $\map{f}{\N}{\Z}$ and just note that it is clearly a 
%   correspondence.
%   The map alternate between positive and negative values:
%   $0\mapsunder{}0$, $1\mapsunder{}1$, $2\mapsunder{}-1$, 
%   $3\mapsunder{}2$, $4\mapsunder{}-2$, $5\mapsunder{}3$, \ldots\,.
%   More formally, $f(n)=-n/2$ if $n$ is even and $f(n)=(n+1)/2$ if~$n$ is odd. 
% \item  As in the prior item we will define a map
%   $\map{f}{\N}{\N\times\N}$ and only note that it is a correspondence, 
%   without going through the verification details.
  
%   The function alternates between columns in the same way that
%   Jackie Chan, when up against multiple bad guys, alternates among his 
%   attackers.
%   Thus $0\mapsunder{} (0,0)$, $1\mapsunder{}(0,1)$, $2\mapsunder{}(1,0)$, 
%   $3\mapsunder{}(0,2)$, 
%   $4\mapsunder{}(1,1)$, $5\mapsunder{}(2,0)$, $6\mapsunder{}(0,3)$, \ldots. 

%   \remark a sketch helps here.
%   The values associated with successive arguments trace out the 
%   diagonals of the array~$\N\times\N$.
%   \begin{center}
%   \newcommand{\chanpoint}[1]{\makebox[0em]{$(#1)$}}
%   \newcommand{\chanarrow}{{\color{darkii} $\searrow$}}
%   \setlength{\unitlength}{0.5in}
%   \begin{picture}(4,4)
%     \put(0,0){\chanpoint{0,0}}
%     \put(1,0){\chanpoint{1,0}}
%     \put(2,0){\chanpoint{2,0}}
%     \put(3,0){\chanpoint{3,0}}
%     \put(3.5,0){\ldots}
%     \put(0,1){\chanpoint{0,1}}
%     \put(1,1){\chanpoint{1,1}}
%     \put(2,1){\chanpoint{2,1}}
%     \put(2.75,1){\ldots}
%     \put(0,2){\chanpoint{0,2}}
%     \put(1,2){\chanpoint{1,2}}
%     \put(2,2){\chanpoint{2,2}}
%     \put(2.75,2){\ldots}
%     \put(0,3){\chanpoint{0,3}}
%     \put(1,3){\chanpoint{1,3}}
%     % \put(2,3){\ldots}
%     \put(0,3.6){$\vdots$}
%     \put(1,3.6){$\vdots$}
%     \put(2.5,3.1){$\iddots$}
%     % arrows
%     \put(0.5,0.5){\chanarrow}
%     \put(0.5,1.5){\chanarrow}
%     \put(1.5,0.5){\chanarrow}
%     \put(0.5,2.5){\chanarrow}
%   \end{picture}
%   \end{center}
% \end{exes}
% \end{ans}
\end{problem}

\begin{problem} \label{ex:OntoFromNatsToSetEquivToCountable}
Prove that the following are equivalent for a set~$A$:
(i)~$A$ is countable,
(ii)~$A$ is empty or there is an onto function from $\N$ to~$A$,
(iii)~there is a one-to-one function from $A$ to~$\N$.    
\begin{answer}
We will show that (i) implies~(ii), which implies~(iii), and which
implies~(i).

First the (i) implies~(ii) proof.
If~$A$ is countable then there are two cases: either~$A$ is finite 
or~$A$ is denumerable.
The case that~$A$ is denumerable by definition has a 
correspondence between~$\N$ and~$A$.
If~$A$ is finite then it is either empty or nonempty.
The empty set is part of statement~(ii)
so suppose that $A$ is finite and nonempty, and let
$a$ be an element of~$A$.
Then there is a correspondence from some initial segment of the natural
numbers~$\map{f}{\set{0,1,\ldots,|A|-1}}{A}$,
so define $\map{\hat{f}}{\N}{A}$ extending~$f$ by
$\hat{f}(i)=f(i)$ if $i\in\set{0,1,\ldots,|A|-1}$, and 
$\hat{f}(i)=a$ if $i>|A|-1$.
This extension~$\hat{f}$ is onto because $f$ is onto.

Next is the proof that statement~(ii) implies~(iii).
By~(ii) either $A$ is the empty set
or there is an onto function from $\N$ to~$A$.
If~$A$ is empty then the empty function is a map with domain~$A$ and
codomain~$\N$ that is one-to-one, vacuously.
If $A$ is nonempty then there is an onto function~$\map{f}{\N}{A}$.
To get a map~$g$ with domain~$A$ and codomain~$\N$ that is one-to-one
define~$g(a)$ to be the least number $n\in\N$ such that~$f(n)=a$; 
at least one such number exists because~$f$ is onto.
Clearly~$g$ is one-to-one.

Finally, the (iii) implies~(i) proof.
Assume that there is a one-to-one function $\map{f}{A}{\N}$.
Let $B=\range(A)\subseteq\N$.
Then the function~$\map{\hat{f}}{A}{B}$ given by~$\hat{f}(a)=f(a)$ is a 
correspondence (it only eliminates unused elements of the codomain).
Thus $A\sim B$.
Because Exercise~\ref{ex:EquinumeruousIsEquivalence} shows that the 
relation~$\sim$ is an equivalence, we will be done if we show that $B$~is
countable.

So suppose that~$B$ is not finite.
Recall that~$B\subseteq\N$ so we can define a map
$\map{g}{\N}{B}$ by:
take $g(j)$ to be the $j$-th largest element of~$B$.
This map is onto because of the assumption that~$B$ is not finite, and it 
is one-to-one by construction:~if $g(j_0)=g(j_1)$ then the $j_0$-th largest
element of~$B$ is the same as the $j_1$-th largest element, which is only
possible if $j_0=j_1$).
\end{answer}
\end{problem}

\begin{problem} 
  Prove that the set of rational numbers is countable.
\begin{answer}
  We will argue that the set of nonnegative rationals
  $Q=\setbuilder{q\in\Q}{q\geq 0}$ is countable.
  With that, a function that alternates sign like the one in 
  Exercise~\ref{ex:IntegersCountable} will count the entire set~$\Q$.

  We will give a procedure to define the enumerating function
  $\map{f}{\N}{\Q^+}$.
  The prior exercise shows that the set $\N\times\N$ is countable;
  let it be counted by~$\map{h}{\N}{\N\times\N}$.
  
  Define $f(0)=0$.
  For $f(1)$, enumerate $h(0)$, $h(1)$, \ldots $h(i_1)$ until there is a value
  $\seq{x_1,y_1}$ such that $y_1\neq 0$ and the fraction $x_1/y_1$
  is not equal to~$0$.
  Such a value must appear because $h$ enumerates $\N\times\N$.
  Set $f(1)=x_1/y_1$.

  Iterate.
  Step~$s+1$ starts with a list $f(0)$, $f(1)$, \ldots{}
  $f(s)$,
  such that $f(s)=x_s/y_s$ where $h(i_s)=\seq{x_s,y_s}$.
  Produce $f(s+1)$ by enumerating $h(i_s+1)$, $h(i_s+2)$, \ldots{}
  until there is a value 
  $h(i_{s+1})=\seq{x_{s+1},y_{s+1}}$ such that $y_{s+1}\neq 0$, and the
  fraction $x_{s+1}/y_{s+1}$ does not appear in the list
  $f(0)$, $f(1)$, \ldots $f(s)$.
  Such a value must appear because $h$ enumerates $\N\times\N$.
  Set $f(s+1)=x_{s+1}/y_{s+1}$.
\end{answer}
\end{problem}

\begin{problem}  Prove that each of these infinite sets is not countable.
\begin{exes}
\begin{exercise} 
  The power set of the naturals, $\powerset(\N)$.
  \hint suppose that $\map{f}{\powerset(\N)}{\N}$
  and consider $\setbuilder{n\in\N}{n\notin f(n)}$.
\end{exercise}
\begin{answer}
  We will prove that no function $\map{f}{\powerset(\N)}{\N}$ is onto.
  Consider $R=\setbuilder{n\in\N}{n\notin f(n)}$.
  Clearly $R\in\powerset(\N)$.
  We will show that there is no $i\in\N$ such that $R=f(i)$.

  For contradiction assume that $R=f(i)$.
  Consider whether $i$ is an element of~$R$.
  Assuming $i\in R$ contradicts the definition of $R$ as the 
  collection of integers~$n$ such that $n\notin f(n)$.
  Assuming $i\notin R$   
  also leads to a contradiction
  because then $i$~satisfies the defining criteria for membership in~$R$, namely
  that $i\notin R$, and therefore $i\in R$.
  Both of the two possibilities are impossible, and so $R\neq f(i)$.
\end{answer}
\begin{exercise} 
  The set of real numbers, $\R$.
  \hint find a one-to-one map from $\powerset(\N)$ to $\R$.
\end{exercise}
\begin{answer}
  We will show that there is a one-to-one 
  map~$\map{f}{\powerset(\N)}{\R}$.
  That gives the result because
  if $\R$ were countable\Dash if there were a correspondence 
  $\map{g}{\R}{\N}$\Dash then 
  that would give an one-to-one function~$\composed{g}{f}$ 
  from $\powerset(\N)$ to $\N$,
  contradicting the prior item and
  Exercise~\ref{ex:OntoFromNatsToSetEquivToCountable} above.

  An input to~$f$ is a set~$A\in\powerset(\N)$.
  We define $f$'s action by
  giving an associated real number output.
  (This output is in the interval $\closed{0}{1}$ but
  that is not relevant to this proof.)
  
  Let the output number be such that its $i$-th decimal place is
  $\charfcn{A}(i)$,
  that is, the $i$-th decimal place is~$1$ if $i\in A$ and is $0$~otherwise. 
  This function is one-to-one because two unequal sets differ in an 
  element~$a\in\N$, and the two image numbers then differ in the $a$-th place.  
\end{answer}
\end{exes}
\end{problem}
